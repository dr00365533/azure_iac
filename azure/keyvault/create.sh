#!/bin/bash

az keyvault create \
	--name $keyvaultName \
	--resource-group $keyvaultResourceGroup

	declare -A defaultValues=( \
	[aksDnsName]="$keyvaultName" \
	[aksKubeVersion]="1.13.5" \
	[aksMaxPods]="110" \
	[aksName]="$keyvaultName" \
	[aksNetworkPlugin]="kubenet" \
	[aksNetworkPrefixes]="10.0.0.0/8" \
	[aksNodeCount]="1" \
	[aksResourceGroup]="keyvaultResourceGroup" \
	[aksServicePrincipalName]="$keyvaultName-aks" \
	[aksSubnetName]="$keyvaultName" \
	[aksSubnetPrefix]="10.2.0.0/22" \
	[aksVmSize]="Standard_F4s_v2" \
	[containerRegistryName]="3rdicr" \
	[containerRegistryResourceGroup]="3rdi-general" \
	[cosmosdbKind]="MongoDB" \
	[cosmosdbLocations]="westeurope=0" \
	[cosmosdbName]="$keyvaultName" \
	[cosmosdbResourceGroup]="keyvaultResourceGroup" \
	[dnsName]="prophes.ee" \
	[keyvaultName]="$keyvaultName" \
	[keyvaultResourceGroup]="keyvaultResourceGroup" \
	[networkName]="prophesee" \
	[networkResourceGroup]="3rdi-general" \
	)

	for key in "${!defaultValues[@]}";
	do
		az keyvault secret set \
		--vault-name $keyvaultName \
		--name "$key" \
		--value ${defaultValues[$key]} \

	done
