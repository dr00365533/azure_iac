#!/bin/bash
aksExists=$(az aks show --resource-group "${AKSRESOURCEGROUP}" --name "${AKSNAME}" --query name -o tsv )
if [ -z "$aksExists" ]
then
az extension add --name aks-preview
aksPublicIPID="$(az network public-ip show --resource-group ${AKSRESOURCEGROUP} --name ${AKSPUBLICIPNAME} --query id -o tsv)"
servicePrincipalId="$(az ad sp show --id "http://${AKSSERVICEPRINCIPALNAME}" --query appId -o tsv)"
servicePrincipalSecret="$(az keyvault secret show --name "${AKSSERVICEPRINCIPALNAME}-secret" --vault-name "${KEYVAULTNAME}" --query value -o tsv)"
subnetId="$(az network vnet subnet show --resource-group "${NETWORKRESOURCEGROUP}" --name "${AKSSUBNETNAME}" --vnet-name "${NETWORKNAME}" --query id --output tsv)"
serverApplicationSecret="$(az keyvault secret show --name "${ADSERVERAPPNAME}-secret" --vault-name "${KEYVAULTNAME}" --query value -o tsv)"
serverApplicationId="$(az ad app list --display-name "${ADSERVERAPPNAME}" --query [].appId -o tsv)"
clientApplicationId="$(az ad app list --display-name "${ADCLIENTAPPNAME}" --query [].appId -o tsv)"

 echo "Creating AKS: "${AKSNAME}""

#Create AAD AKS
az aks create \
	--resource-group "${AKSRESOURCEGROUP}" \
	--name "${AKSNAME}" \
	--generate-ssh-keys \
	--vm-set-type "${AKSVMTYPE}"  \
	--enable-cluster-autoscaler \
	--node-count "${AKSNODECOUNT}" \
	--min-count "${AKSMINNODECOUNT}" \
	--max-count "${AKSMAXNODECOUNT}" \
	--service-principal "${servicePrincipalId}" \
	--client-secret "${servicePrincipalSecret}" \
	--max-pods "${AKSMAXPODS}" \
	--dns-name-prefix "${AKSDNSPREFIX}" \
	--kubernetes-version "${AKSKUBEVERSION}" \
	--network-plugin "${AKSNETWORKPLUGIN}" \
	--vnet-subnet-id "${subnetId}" \
	--node-vm-size "${AKSVMSIZE}" \
	--aad-server-app-id "${serverApplicationId}" \
	--aad-server-app-secret "${serverApplicationSecret}" \
	--aad-client-app-id "${clientApplicationId}" \
	--aad-tenant-id "${PROPHESEETENANTID}" \
	--load-balancer-sku "${AKSLBSKU}" \
	--load-balancer-outbound-ips "${aksPublicIPID}" \
	--enable-addons monitoring  \
	--workspace-resource-id "${AZUREMONITORID}" \
	--nodepool-name "prophesee" \
	--node-resource-group "${AKSNODERESOURCEGROUP}" \
	--zones 1 2 3 \
	--tags "stage=prod" "product=prophesee"

	az aks update --name "${AKSNAME}" --resource-group "${AKSRESOURCEGROUP}" --attach-acr "${CONTAINERREGISTRYNAME}"
else
  echo "AKS: "${AKSNAME}" exists skipping"
fi
