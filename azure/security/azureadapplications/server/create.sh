#!/bin/bash

serverAppExists=$(az ad app show --id http://"${ADSERVERAPPNAME}" --query appId -o tsv )

if [ -z "$serverAppExists" ]
then
	echo "Creating AAD Server Application for AKS"
	#Create AAD Server Application
	serverUri="http://${ADSERVERAPPNAME}"
	serverApplicationSecret=$(gpg --gen-random --armor 1 32)
	serverAppApplicationId=$(az ad app create --display-name "${ADSERVERAPPNAME}" \
	--identifier-uris "${serverUri}" \
	--end-date "2050-1-1" \
	--password "${serverApplicationSecret}" \
	--reply-urls "${serverUri}" \
	--homepage "${serverUri}" \
	--query appId -o tsv)
	az ad app update --id "${serverAppApplicationId}" --set groupMembershipClaims=All
	az ad app permission add --id "${serverAppApplicationId}" --api 00000003-0000-0000-c000-000000000000 --api-permissions e1fe6dd8-ba31-4d61-89e7-88639da4683d=Scope 06da0dbc-49e2-44d2-8312-53f166ab848a=Scope 7ab1d382-f21e-4acd-a863-ba3e13f7da61=Role
	az ad sp create --id "${serverAppApplicationId}"
	az ad app permission grant --id "${serverAppApplicationId}" --api 00000003-0000-0000-c000-000000000000

	#Save secret in Keyvault
	az keyvault secret set --vault-name "${KEYVAULTNAME}" --name "${ADSERVERAPPNAME}-secret" --value "${serverApplicationSecret}"

	echo "AAD: "${ADSERVERAPPNAME}" created"

else
	echo "AAD Server: "${ADSERVERAPPNAME}" exists skipping"
fi