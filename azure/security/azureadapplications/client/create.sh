clientAppExists=$(az ad app list --display-name "${ADCLIENTAPPNAME}" --query [].appId -o tsv)

if [ -z "$clientAppExists" ]
then
	echo "Creating AAD Client Application for AKS"
	#Create AAD Client Application
	serverAppApplicationId=$(az ad app list --display-name ${ADSERVERAPPNAME} --query [].appId -o tsv)
	oAuthPermissionId=$(az ad app show --id "${serverAppApplicationId}" --query "oauth2Permissions[0].id" -o tsv)
	clientApplicationId=$(az ad app create --display-name "${ADCLIENTAPPNAME}" --native-app --reply-urls "http://${ADCLIENTAPPNAME}" --query appId -o tsv)
	az ad app permission add --id $clientApplicationId --api "${serverAppApplicationId}" --api-permissions $oAuthPermissionId=Scope --subscription "${PROPHESEESUBSCRIPTIONID}"
	az ad sp create --id ${clientApplicationId}

	echo "AAD: "${ADCLIENTAPPNAME}" created"

else
	echo "AAD Client: "${ADCLIENTAPPNAME}" exists skipping"
fi
