#!/bin/bash
identityExists=$(az identity show -g "${AKSRESOURCEGROUP}" -n "${AKSPODIDENTITYNAME}" --query id --output tsv )
if [ -z "$identityExists" ]
then
  echo "Creating "${AKSPODIDENTITYNAME}" binding for "${AKSNAME}""
	#Azure Identity ID
	identityId=$(az identity create -g "${AKSRESOURCEGROUP}" -n "${AKSPODIDENTITYNAME}" -o json --query principalId --output tsv)
	identityClientId=$(az identity show -g "${AKSRESOURCEGROUP}" -n "${AKSPODIDENTITYNAME}" --query clientId --output tsv)
	sleep 120
	#Create Azure Identity Role Read for AKS ResrouceGroup
	az role assignment create --role Reader --assignee "${identityId}" --scope "/subscriptions/"${PROPHESEESUBSCRIPTIONID}"/resourcegroups/"${AKSRESOURCEGROUP}""
	#Create Azure Identity Role
	az role assignment create --role Reader --assignee "${identityId}" --scope "/subscriptions/"${PROPHESEESUBSCRIPTIONID}"/resourcegroups/"${KEYVAULTRESOURCEGROUP}""
	# set policy to access keys in your keyvault
	az keyvault set-policy -n "${KEYVAULTNAME}" --key-permissions get --spn "${identityClientId}"
	# set policy to access secrets in your keyvault
	az keyvault set-policy -n "${KEYVAULTNAME}" --secret-permissions get --spn "${identityClientId}"
	# set policy to access certs in your keyvault
	az keyvault set-policy -n "${KEYVAULTNAME}" --certificate-permissions get --spn "${identityClientId}"
	#Get Azure Identity Role Scope
	identityScope=$(az identity show -g "${AKSRESOURCEGROUP}" -n "${AKSPODIDENTITYNAME}" --query id --output tsv)
	#Set Scope to AKS Service Principal
	az role assignment create --role "Managed Identity Operator" --assignee "http://${AKSSERVICEPRINCIPALNAME}" --scope $identityScope
else
  echo "AAD-Pod-Identity: "${AKSPODIDENTITYNAME}" exists skipping"
fi
