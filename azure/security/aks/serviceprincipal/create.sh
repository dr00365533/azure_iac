#!/bin/bash

servicePrincipalExists=$(az ad sp show --id "http://${AKSSERVICEPRINCIPALNAME}" --query appId -o tsv )
if [ -z "${servicePrincipalExists}" ]
then
  echo "Creating serviceprincipal for AKS"
  servicePrincipalPassword=$(az ad sp create-for-rbac --name "${AKSSERVICEPRINCIPALNAME}" --skip-assignment --query password -o tsv)
  servicePrincipalId=$(az ad sp show --id "http://${AKSSERVICEPRINCIPALNAME}" --query appId -o tsv)
  az keyvault secret set --vault-name "${KEYVAULTNAME}" --name "${AKSSERVICEPRINCIPALNAME}-secret" --value $servicePrincipalPassword
  sleep 30
  #Add Principal to network
  echo "Setting permissions to Network: ${NETWORKNAME}"
  vnetScope=$(az network vnet show --resource-group "${NETWORKRESOURCEGROUP}" --name ${NETWORKNAME} --query id -o tsv)
  #Vnet Permissions
  az role assignment create --assignee "${servicePrincipalId}" --scope "${vnetScope}" --role Contributor
  #Add Principal to Keyvault
  keyvaultScope=$(az keyvault show --resource-group "${KEYVAULTRESOURCEGROUP}" --name "${KEYVAULTNAME}" --query id -o tsv)
  #Keyvault Permissions
  echo "Setting permissions to Keyvault: ${KEYVAULTNAME}"
  az role assignment create --role Reader --assignee "${servicePrincipalId}" --scope "${keyvaultScope}"
  az keyvault set-policy -n "${KEYVAULTNAME}" --secret-permissions get --spn "${servicePrincipalId}"
  az keyvault set-policy -n "${KEYVAULTNAME}" --certificate-permissions get --spn "${servicePrincipalId}"
  az keyvault set-policy -n "${KEYVAULTNAME}" --key-permissions get --spn "${servicePrincipalId}"
  #Add Principal to ACR
  acrId=$(az acr show --name "${CONTAINERREGISTRYNAME}" --resource-group "${CONTAINERREGISTRYRESOURCEGROUP}" --query id --output tsv)
  #Container Registry Permissions
  echo "Setting permissions to Container Registry: "${CONTAINERREGISTRYNAME}""
  az role assignment create --assignee "${servicePrincipalId}" --role Reader --scope $acrId
  #Add Principal to DNS Zone
  dnsScope=$(az network dns zone show -g "${DNSRESOURCEGROUP}" -n "${DNSNAME}" --query id -o tsv)
  #DNS Registry Permissions
  echo "Setting permissions to DNS Zone: "${DNSNAME}""
  az role assignment create --assignee "${servicePrincipalId}" --role Contributor --scope "${dnsScope}"
  echo "Service Principal ${AKSSERVICEPRINCIPALNAME} created"
else
  echo "ServicePrincipal: ${AKSSERVICEPRINCIPALNAME} exists skipping"
fi
