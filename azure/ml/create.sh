#!/bin/bash
az extension add -n azure-cli-ml
mlWorkspaceName="${propheseeResourceGroup}-${propheseeStage}-ml"

mlExists=$(az ml workspace show --resource-group "${propheseeResourceGroup}" --name "${mlWorkspaceName}" --query name -o tsv)
if [ -z "$mlExists" ]
then
  #Create Storage account
  az storage account create --name "{$propheseeResourceGroup}{$propheseeStage}ml" \
  --resource-group "${propheseeResourceGroup}"

  #Create Application Insight?
  #Create Container-Registry?
  #Create Keyvault?

  az ml workspace create --workspace-name "${mlWorkspaceName}" \
  --application-insights "${mlWorkspaceName}" \
  --container-registry "${mlWorkspaceName}" \
  --friendly-name "${mlWorkspaceName}" \
  --keyvault "${mlWorkspaceName}" \
  --location "westeurope" \
  --resource-group "$propheseeResourceGroup" \
  --storage-account "/subscriptions/$subscriptionId/resourceGroups/$propheseeResourceGroup/providers/Microsoft.Storage/storageAccounts/${propheseeResourceGroup}${propheseeStage}ml"
else
  echo "Network exists continuing"
fi
