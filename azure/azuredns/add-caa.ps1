Login-AzureRmAccount

#select subscription with Azure DNS
Get-AzureRmSubscription
Set-AzureRmContext -SubscriptionId "72fd34a7-826d-4fbf-bbd3-a836ff9eb9a6"

#List all ResourceGroups in Subscription
Get-AzureRmResourceGroup

#List all DNS zones in ResourceGroup
Get-AzureRmDnsZone -ResourceGroupName 'networking-3rdi'

#List all CAA records for DNS zone
Get-AzureRmDnsRecordSet -ResourceGroupName 'networking-3rdi' -ZoneName 'prophes.ee' -RecordType CAA

#Preparing records
$caaRecords = @()
$caaRecords += New-AzureRmDnsRecordConfig -CaaFlag "0" -CaaTag "iodef" -CaaValue "mailto:akshay@3rdi.ai"
$caaRecords += New-AzureRmDnsRecordConfig -CaaFlag "0" -CaaTag "issue" -CaaValue "letsencrypt.org"
$caaRecords += New-AzureRmDnsRecordConfig -CaaFlag "0" -CaaTag "issuewild" -CaaValue "letsencrypt.org"

#Adding CAA records
New-AzureRmDnsRecordSet -Name "@" -RecordType CAA -ZoneName 'prophes.ee' -ResourceGroupName 'networking-3rdi' -Ttl 3600 -DnsRecords $caaRecords

#Verify CAA records are added in the DNS zone
Get-AzureRmDnsRecordSet -ResourceGroupName 'networking-3rdi' -ZoneName prophes.ee -RecordType CAA