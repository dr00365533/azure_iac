#!/bin/bash
dbExists=$(az cosmosdb check-name-exists --name "${cosmosdbName}" --subscription $subscriptionId)

if [ $dbExists == false ]
then
	echo "Creating CosmosDB"
	az cosmosdb create \
	--name ""${cosmosdbName}"" \
	--resource-group "$cosmosdbResourceGroup" \
	--locations "$cosmosdbLocations" \
	--kind "$cosmosdbKind" \
	--tags "customer=$customer" "product=$product"


	echo "Creating Prophesee Database"
	cosmosdbThroughput="400"
	az cosmosdb database create \
	--db-name "prophesee" \
	--name "${cosmosdbName}" \
	--resource-group "$cosmosdbResourceGroup" \
	--throughput "$cosmosdbThroughput"

	for collection in "customer360" "forecasts" "opportunities" "pages" "rules" "settings";
	do
	echo "Creating $collection collection"
	az cosmosdb collection create \
	--collection-name "$collection" \
	--db-name "prophesee" \
	--name "${cosmosdbName}" \
	--resource-group "$cosmosdbResourceGroup" \
	--partition-key-path "/clientId"
	done

	sleep 20
	echo "Updating CosmosDB firewall"
	subnetId=$(az network vnet subnet list --resource-group "$networkResourceGroup" --vnet-name "$networkName" --query [].id --output tsv)
	az cosmosdb network-rule add --resource-group "$cosmosdbResourceGroup" --name "${cosmosdbName}"  --subnet "$subnetId"


else
	echo "CosmosDB: "${cosmosdbName}" exists skipping"
fi
