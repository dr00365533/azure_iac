name: '$(Date:yyyyMMdd)$(Rev:.r)'
resources:
- repo: self
trigger: none
variables:
- name: vmImageName
  value: ubuntu-latest
- name: product
  value: prophesee
- name: customer
  value: saas
stages:
- stage: Dev
  variables:
  - group: prophesee-saas-dev
  - group: prophesee-saas-general
  displayName: Setup Dev Environment
  jobs:
  - job: AAD_AKS_Principals
    displayName: Create AAD/AKS Service Principals
    pool:
      vmImage: $(vmImageName)
    steps:
    - checkout: self

    - task: AzureCLI@1
      enabled: 'true'
      displayName: Create AAD Server/Client Principals
      inputs:
        azureSubscription: 'Prophesee-saas-dev'
        scriptLocation: 'scriptPath'
        scriptPath: '$(Build.SourcesDirectory)/security/createaadapps.sh'
      env:
        keyvaultName: $(keyvaultName)
        subscriptionId: $(subscriptionId)
        serverAppName: $(AKSAADServerName)
        clientAppName: $(AKSAADClientName)

    - task: AzureCLI@1
      enabled: 'true'
      displayName: Create AKS ServicePrincipal
      inputs:
        azureSubscription: 'Prophesee-saas-dev'
        scriptLocation: 'scriptPath'
        scriptPath: '$(Build.SourcesDirectory)/security/serviceprincipal.sh'
      env:
        aksServicePrincipalName: $(aksServicePrincipalName)
        keyvaultName: $(keyvaultName)
        keyvaultResourceGroup: $(keyvaultResourceGroup)
        networkName: $(networkName)
        networkResourceGroup: $(networkResourceGroup)
        containerRegistryName: $(containerRegistryName)
        containerRegistryResourceGroup: $(containerRegistryResourceGroup)
        dnsName: $(dnsName)

  - job: network
    dependsOn: AAD_AKS_Principals
    displayName: Create AKS network
    pool:
      vmImage: $(vmImageName)
    steps:
    - checkout: self

    - task: AzureCLI@1
      enabled: 'true'
      displayName: Create network and/or subnet
      inputs:
        azureSubscription: 'Prophesee-saas-dev'
        scriptLocation: 'scriptPath'
        scriptPath: '$(Build.SourcesDirectory)/network/create.sh'
      env:
        product: $(product)
        customer: $(customer)
        networkName: $(networkName)
        networkResourceGroup: $(networkResourceGroup)
        aksSubnetName: $(aksSubnetName)
        aksNetworkPrefixes: $(aksNetworkPrefixes)
        aksSubnetPrefix: $(aksSubnetPrefix)

  - job: cosmosdb
    dependsOn: network
    displayName: Create Cosmos DB
    pool:
      vmImage: $(vmImageName)
    steps:
    - checkout: self

    - task: AzureCLI@1
      enabled: 'true'
      displayName: Create CosmosDB
      inputs:
        azureSubscription: 'Prophesee-saas-dev'
        scriptLocation: 'scriptPath'
        scriptPath: '$(Build.SourcesDirectory)/cosmosdb/create.sh'
      env:
        product: $(product)
        customer: $(customer)
        subscriptionId: $(subscriptionId)
        servicePrincipalName: $(aksServicePrincipalName)
        cosmosdbName: $(cosmosdbName)
        cosmosdbResourceGroup: $(cosmosdbResourceGroup)
        cosmosdbLocations: $(cosmosdbLocations)
        cosmosdbKind: $(cosmosdbKind)
        networkName: $(networkName)
        networkResourceGroup: $(networkResourceGroup)


  - job: aks
    dependsOn: cosmosdb
    displayName: Create AKS
    pool:
      vmImage: $(vmImageName)
    steps:
    - checkout: self

    - task: AzureCLI@1
      enabled: 'true'
      displayName: Create AKS
      inputs:
        azureSubscription: 'Prophesee-saas-dev'
        scriptLocation: 'scriptPath'
        scriptPath: '$(Build.SourcesDirectory)/aks/create.sh'
      env:
        product: $(product)
        customer: $(customer)
        tenantId: $(tenantId)
        subscriptionId: $(subscriptionId)
        keyvaultName: $(keyvaultName)
        networkName: $(networkName)
        networkResourceGroup: $(networkResourceGroup)
        aksSubnetName: $(aksSubnetName)
        maxPods: $(aksMaxPods)
        servicePrincipalName: $(aksServicePrincipalName)
        keyvaultResourceGroup: $(keyvaultResourceGroup)
        containerRegistryName: $(containerRegistryName)
        containerRegistryResourceGroup: $(containerRegistryResourceGroup)
        dnsName: $(aksDnsName)
        kubeVersion: $(aksKubeVersion)
        vmSize: $(aksVmSize)
        nodeCount: $(aksNodeCount)
        aksResourceGroup: $(aksResourceGroup)
        aksName: $(aksName)
        networkPlugin: $(aksNetworkPlugin)
        serverAppName: $(AKSAADServerName)
        clientAppName: $(AKSAADClientName)

  - job: base_aks_setup
    displayName: Setup Base Kubernetes
    dependsOn: aks
    pool:
      vmImage: $(vmImageName)
    steps:
      - checkout: self

      - task: KubectlInstaller@0
        inputs:
          kubectlVersion: 'latest'

      - task: AzureCLI@1
        enabled: 'true'
        displayName: Base Setup
        inputs:
          azureSubscription: 'Prophesee-saas-dev'
          scriptLocation: 'scriptPath'
          scriptPath: '$(Build.SourcesDirectory)/charts/setup/install-base.sh'
          useGlobalConfig: true
        env:
          aksServicePrincipalName: $(aksServicePrincipalName)
          aksPropheseeNamespace: $(aksPropheseeNamespace)
          keyvaultName: $(keyvaultName)
          aksResourceGroup: $(aksResourceGroup)
          aksName: $(aksName)
          directory: $(Build.SourcesDirectory)

      - task: AzureCLI@1
        enabled: 'true'
        displayName: Setup Jaeger operator
        inputs:
          azureSubscription: 'Prophesee-saas-dev'
          scriptLocation: 'scriptPath'
          scriptPath: '$(Build.SourcesDirectory)/charts/setup/install-jaeger-operator.sh'
          useGlobalConfig: true
        env:
          directory: $(Build.SourcesDirectory)

      - task: AzureCLI@1
        enabled: 'true'
        displayName: Setup Prometheus operator
        inputs:
          azureSubscription: 'Prophesee-saas-dev'
          scriptLocation: 'scriptPath'
          scriptPath: '$(Build.SourcesDirectory)/charts/setup/install-prometheus-operator.sh'
          useGlobalConfig: true
        env:
          directory: $(Build.SourcesDirectory)

      - task: AzureCLI@1
        enabled: 'true'
        displayName: Setup Cert-Manager
        inputs:
          azureSubscription: 'Prophesee-saas-dev'
          scriptLocation: 'scriptPath'
          scriptPath: '$(Build.SourcesDirectory)/charts/setup/install-certmanager.sh'
          useGlobalConfig: true
        env:
          directory: $(Build.SourcesDirectory)
          aksServicePrincipalName: $(aksServicePrincipalName)
          propheseeStage: $(propheseeStage)

  - job: install_istio
    displayName: Setup Istio
    dependsOn: base_aks_setup
    pool:
      vmImage: $(vmImageName)
    steps:
      - checkout: self

      - task: AzureCLI@1
        enabled: 'true'
        displayName: Setup Istio
        inputs:
          azureSubscription: 'Prophesee-saas-dev'
          scriptLocation: 'scriptPath'
          scriptPath: '$(Build.SourcesDirectory)/charts/setup/install-istio.sh'
          useGlobalConfig: true
        env:
          directory: $(Build.SourcesDirectory)
          aksResourceGroup: $(aksResourceGroup)
          aksName: $(aksName)
          aksServicePrincipalName: $(aksServicePrincipalName)

  - job: install_authentication
    displayName: Setup Authentication/Authorization
    dependsOn: install_istio
    pool:
      vmImage: $(vmImageName)
    steps:
      - checkout: self

      - task: AzureCLI@1
        enabled: 'true'
        displayName: Setup Session Redis
        inputs:
          azureSubscription: 'Prophesee-saas-dev'
          scriptLocation: 'scriptPath'
          scriptPath: '$(Build.SourcesDirectory)/charts/setup/install-redis-ha.sh'
          useGlobalConfig: true
        env:
          directory: $(Build.SourcesDirectory)
          aksResourceGroup: $(aksResourceGroup)
          aksName: $(aksName)
          propheseeStage: $(propheseeStage)

  - job: install_ambassador
    displayName: Setup Ambassador
    dependsOn: install_authentication
    pool:
      vmImage: $(vmImageName)
    steps:
      - checkout: self

      - task: AzureCLI@1
        enabled: 'true'
        displayName: Setup Ambassador
        inputs:
          azureSubscription: 'Prophesee-saas-dev'
          scriptLocation: 'scriptPath'
          scriptPath: '$(Build.SourcesDirectory)/charts/setup/install-ambassador.sh'
          useGlobalConfig: true
        env:
          directory: $(Build.SourcesDirectory)
          aksResourceGroup: $(aksResourceGroup)
          aksName: $(aksName)
          propheseeStage: $(propheseeStage)
