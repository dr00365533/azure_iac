#!/bin/bash
acrExists=$(az acr show --resource-group "${CONTAINERREGISTRYRESOURCEGROUP}" --name "${CONTAINERREGISTRYNAME}" --query name -o tsv )

if [ -z "$acrExists" ]
then
  echo "Creating Azure Container Registry: "${CONTAINERREGISTRYNAME}""

az acr create
	--name  "${CONTAINERREGISTRYNAME}" \
	--resource-group "${CONTAINERREGISTRYRESOURCEGROUP}"
	--sku "${CONTAINERREGISTRYSKU}"

else
  echo "Azure Container Registry: "${CONTAINERREGISTRYNAME}" exists skipping"
fi
