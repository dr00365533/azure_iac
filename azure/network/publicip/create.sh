#!/bin/bash
ipExists=$(az network public-ip show --resource-group "${AKSNODERESOURCEGROUP}" --name "${AKSPUBLICIPNAME}"  --query name -o tsv )

if [ -z "$ipExists" ]
then
  echo "Creating Public IP: "${AKSPUBLICIPNAME}""
  az network public-ip create \
      --resource-group "${AKSRESOURCEGROUP}" \
      --name "${AKSPUBLICIPNAME}" \
      --sku "${AKSLBSKU}" \
      --allocation-method static

else
  echo "Public IP Exists: "${AKSPUBLICIPNAME}" exists skipping"
fi
