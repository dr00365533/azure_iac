#!/bin/bash
subnetExists=$(az network vnet subnet show --resource-group "${NETWORKRESOURCEGROUP}" --name "${AKSSUBNETNAME}" --vnet-name "${NETWORKNAME}" --query name -o tsv )

if [ -z "$subnetExists" ]
then
  echo "Creating subnet: "${AKSSUBNETNAME}""

  az network vnet subnet create \
  --address-prefixes "${AKSSUBNETPREFIX}" \
  --name "${AKSSUBNETNAME}" \
  --resource-group "${NETWORKRESOURCEGROUP}" \
  --vnet-name "${NETWORKNAME}"

	echo "Updating subnet endpoints"

  az network vnet subnet update \
  --resource-group "${NETWORKRESOURCEGROUP}" \
  --name "${AKSSUBNETNAME}" \
  --vnet-name "${NETWORKNAME}" \
  --service-endpoints Microsoft.AzureCosmosDB Microsoft.AzureActiveDirectory Microsoft.ContainerRegistry Microsoft.KeyVault
else
  echo "Subnet: "${AKSSUBNETNAME}" exists skipping"
fi
