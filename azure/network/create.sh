#!/bin/bash
networkExists=$(az network vnet show --resource-group "${NETWORKRESOURCEGROUP}" --name "${NETWORKNAME}" --query name -o tsv )
if [ -z "${networkExists}" ]
then
  echo "Creating network: "${NETWORKNAME}""
  az network vnet create \
  --resource-group  "${NETWORKRESOURCEGROUP}" \
  --name "${NETWORKNAME}" \
  --address-prefixes "${NETWORKPREFIXES}"
else
  echo "Network: "${NETWORKNAME}" exists skipping"
fi
