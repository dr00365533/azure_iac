#Global
directory="${PWD}"
propheseeStage="prod"
propheseeGeneralKVName="PropheseeProd0001"

#Keyvault
keyvaultName="PropheseeProd0001"
keyvaultResourceGroup="PropheseeSecurity"

#Azure Kubernetes Service
aksResourceGroup="PropheseeKubernetes"
aksName="prophesee"
aksServicePrincipalName="PropheseeProdAKS"

#Identity Paramters
podIdentityName="${aksName}-aks-pod-identity"

#Setup Kubernetes Base
. ./kubernetes/setup/install-base.sh

#Setup AAD Pod Identity
. ./kubernetes/setup/install-aad-pod-identity.sh

#Setup Cert-Manager
# . ./kubernetes/setup/install-certmanager.sh

#Setup Jaeger Operator
# . ./kubernetes/setup/install-jaeger-operator.sh

#Setup Prometheus Operator
# . ./kubernetes/setup/install-prometheus-operator.sh

#Setup Prometheus
# . ./kubernetes/setup/install-prometheus.sh

# Setup Jaeger
# . ./kubernetes/setup/install-jaeger.sh

# Setup Grafana
# . ./kubernetes/setup/install-grafana.sh

