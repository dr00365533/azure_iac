#!/bin/bash
servicePrincipalId=$(az ad sp show --id "http://${aksServicePrincipalName}" --query appId -o tsv)
servicePrincipalSecret=$(az keyvault secret show --name "${aksServicePrincipalName}-secret" --vault-name "${keyvaultName}" --query value -o tsv)
az aks get-credentials --resource-group "${aksResourceGroup}" --name "${aksName}" --admin --overwrite-existing
helm init --service-account tiller --client-only --wait
helm repo add jetstack https://charts.jetstack.io
echo "Installing Cert-Manager"
kubectl apply -f https://raw.githubusercontent.com/jetstack/cert-manager/release-0.10/deploy/manifests/00-crds.yaml
# Install Cert-manager
helm upgrade --install certmanager  jetstack/cert-manager --namespace cert-manager --version v0.10.0 --wait
#Add Certmanager secret for Azure DNS integration
kubectl create secret generic azuredns-config --from-literal=client-secret="${servicePrincipalSecret}" --namespace cert-manager
helm upgrade --install prophesee-certificates  "${directory}/kubernetes/prophesee-certificates/" --namespace gateway --set azuredns.clientId="${servicePrincipalId}"  --wait
