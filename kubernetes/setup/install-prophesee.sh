directory="/mnt/c/projects/prophesee/"
propheseeStage="dev"
helm upgrade --install prophesee "${directory}/prophesee-nuxt/kubernetes/prophesee" --namespace "${propheseeStage}" --wait
helm upgrade --install accounts "${directory}/accounts/kubernetes/accounts" --namespace "${propheseeStage}" --wait
helm upgrade --install integrations "${directory}/integrations/kubernetes/integrations" --namespace "${propheseeStage}" --wait
helm upgrade --install rules "${directory}/rules/kubernetes/rules" --namespace "${propheseeStage}" --wait
helm upgrade --install planner "${directory}/planner/kubernetes/planner" --namespace "${propheseeStage}" --wait
helm upgrade --install settings "${directory}/settings/kubernetes/settings" --namespace "${propheseeStage}" --wait
helm upgrade --install authentication "/mnt/c/users/PerAndersLjusbäck/go/src/authentication/kubernetes/authentication" --namespace "${propheseeStage}" --wait
