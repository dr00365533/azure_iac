#!/bin/bash
az aks get-credentials --resource-group "${aksResourceGroup}" --name "${aksName}" --admin --overwrite-existing
echo "Installing Jaeger Operator"
helm upgrade --install jaeger-operator  "${directory}/kubernetes/jaeger-operator" --namespace jaeger-operator --wait
