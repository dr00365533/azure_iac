#!/bin/bash
az aks get-credentials --resource-group "${aksResourceGroup}" --name "${aksName}" --admin --overwrite-existing
echo "Installing Prometheus"
kubectl create namespace observability
helm upgrade --install prometheus stable/prometheus-operator --namespace observability  \
--set prometheusOperator.enabled=false \
--set prometheus.prometheusSpec.serviceMonitorSelector.any=true \
--set prometheus.prometheusSpec.serviceMonitorNamespaceSelector.any=true \
--set grafana.enabled=false
