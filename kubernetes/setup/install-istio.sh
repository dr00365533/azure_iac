#!/bin/bash
echo "Installing Istio"
servicePrincipalId=$(az ad sp show --id "http://${aksServicePrincipalName}" --query appId -o tsv)
az aks get-credentials --resource-group "${aksResourceGroup}" --name "${aksName}" --admin --overwrite-existing
# helm init --service-account tiller --client-only --wait
# helm upgrade --install istio-cni "${directory}/kubernetes/istio-operator/charts/istio-cni/"  -f "${directory}/kubernetes/istio-operator/charts/istio-cni/values.yaml" --namespace istio-cni  --wait
kubectl create namespace gateway
kubectl create secret generic kvcreds --from-literal clientid="${servicePrincipalId}" --from-literal clientsecret="${servicePrincipalSecret}" --type="azure/kv" -n gateway

${directory}/kubernetes/istio-operator/bin/istioctl manifest apply -f "${directory}/kubernetes/istio-operator/prophesee/production.yaml"
# kubectl apply -f "${directory}/kubernetes/prometheus-servicemonitors/istio.yaml"
