#!/bin/bash
az aks get-credentials --resource-group "${aksResourceGroup}" --name "${aksName}" --admin --overwrite-existing
echo "Installing Prometheus Operator"
kubectl apply -f https://raw.githubusercontent.com/coreos/prometheus-operator/master/example/prometheus-operator-crd/alertmanager.crd.yaml
kubectl apply -f https://raw.githubusercontent.com/coreos/prometheus-operator/master/example/prometheus-operator-crd/prometheus.crd.yaml
kubectl apply -f https://raw.githubusercontent.com/coreos/prometheus-operator/master/example/prometheus-operator-crd/prometheusrule.crd.yaml
kubectl apply -f https://raw.githubusercontent.com/coreos/prometheus-operator/master/example/prometheus-operator-crd/servicemonitor.crd.yaml
kubectl apply -f https://raw.githubusercontent.com/coreos/prometheus-operator/master/example/prometheus-operator-crd/podmonitor.crd.yaml

helm install --name prometheus-operator stable/prometheus-operator --namespace prometheus-operator  \
--set prometheus.enabled=false \
--set alertmanager.enabled=false \
--set grafana.enabled=false \
--set coreDns.enabled=false \
--set kubeDns.enabled=false \
--set kubeProxy.enabled=false \
--set kubeEtcd.enabled=false \
--set kubeApiServer.enabled=false \
--set kubeScheduler.enabled=false \
--set kubelet.enabled=false \
--set nodeExporter.enabled=false \
--set kubeControllerManager.enabled=false \
--set prometheusOperator.createCustomResource=false \
--set kubeStateMetrics.enabled=false
