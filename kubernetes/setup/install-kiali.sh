#!/bin/bash
az aks get-credentials --resource-group "${aksResourceGroup}" --name "${aksName}" --admin --overwrite-existing
echo "Installing Kiali Operator"
# #Setup Kiali Operator
kubectl create namespace observability
kubectl apply -f  "${directory}/kubernetes/kiali/prophesee-kiali-cr.yaml" --namespace observability
