#!/bin/bash
echo "Installing Ambassador Gateway"
az aks get-credentials --resource-group "${aksResourceGroup}" --name "${aksName}" --admin --overwrite-existing
helm upgrade --install ambassador  "${directory}/kubernetes/ambassador" --namespace gateway --wait
