#!/bin/bash
servicePrincipalId=$(az ad sp show --id "http://${aksServicePrincipalName}" --query appId -o tsv)
servicePrincipalSecret=$(az keyvault secret show --name "${aksServicePrincipalName}-secret" --vault-name "${keyvaultName}" --query value -o tsv)
echo "Installing Base Kubernetes Setup"
az aks get-credentials --resource-group "${aksResourceGroup}" --name "${aksName}" --admin --overwrite-existing

#Setup AKS Dashboard
kubectl create -f "${directory}/kubernetes/aks-dashboard/clusterrole.yaml"

#Setup Azure Keyvault Integrations
kubectl apply -f https://raw.githubusercontent.com/Azure/kubernetes-keyvault-flexvol/master/deployment/kv-flexvol-psp.yaml
kubectl create -f https://raw.githubusercontent.com/Azure/kubernetes-keyvault-flexvol/master/deployment/kv-flexvol-installer.yaml

#Setup Azure AD integration to AKS
helm upgrade --install aadrbac "${directory}/kubernetes/aadrbac" --namespace default --wait
