#!/bin/bash
echo "Installing Istio"
servicePrincipalId=$(az ad sp show --id "http://${aksServicePrincipalName}" --query appId -o tsv)
az aks get-credentials --resource-group "${aksResourceGroup}" --name "${aksName}" --admin --overwrite-existing
kubectl create namespace gateway
kubectl create namespace istio-system
kubectl create secret generic kvcreds --from-literal clientid="${servicePrincipalId}" --from-literal clientsecret="${servicePrincipalSecret}" --type="azure/kv" -n gateway
${directory}/kubernetes/istio-operator/bin/istioctl manifest apply -f "${directory}/kubernetes/istio-operator/prophesee/authentication-prod.yaml"
