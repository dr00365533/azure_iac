#!/bin/bash
az aks get-credentials --resource-group "${aksResourceGroup}" --name "${aksName}" --admin --overwrite-existing
echo "Installing Jaeger Operator"
#Setup Jaeger
kubectl create namespace observability
kubectl apply -f  "${directory}/kubernetes/jaeger/jaeger-setup.yaml" --namespace observability
