#!/bin/bash
az aks get-credentials --resource-group "${aksResourceGroup}" --name "${aksName}" --admin --overwrite-existing
identityScope=$(az identity show -g "${aksResourceGroup}" -n "${podIdentityName}" --query id --output tsv)
identityClientId=$(az identity show -g "${aksResourceGroup}" -n "${podIdentityName}" --query clientId --output tsv)
echo "Installing AAD-Pod-Identity"

helm upgrade \
--install aadpodidentity  "${directory}/kubernetes/aad-pod-identity" \
--set azureIdentity.resourceID="${identityScope}" \
--set azureIdentity.clientID="${identityClientId}" --wait
