#!/bin/bash
servicePrincipalId=$(az ad sp show --id "http://${aksServicePrincipalName}" --query appId -o tsv)
servicePrincipalSecret=$(az keyvault secret show --name "${aksServicePrincipalName}-secret" --vault-name "${keyvaultName}" --query value -o tsv)
echo "Installing ${propheseeStage} Kubernetes Setup"
az aks get-credentials --resource-group "${aksResourceGroup}" --name "${aksName}" --admin --overwrite-existing

# Azure KeyVault Integration Secret
kubectl create secret generic kvcreds --from-literal clientid="${servicePrincipalId}" --from-literal clientsecret="${servicePrincipalSecret}" --type="azure/kv" -n "${propheseeStage}"

#Setup Namespace
kubectl create namespace "${propheseeStage}"
kubectl label namespace "${propheseeStage}" istio-env="istio-control" name=prophesee --overwrite

