#!/bin/bash
az aks get-credentials --resource-group "${aksResourceGroup}" --name "${aksName}" --admin --overwrite-existing
echo "Installing Session Redis Cache"
kubectl create namespace security
helm upgrade --install "prophesee-session-${propheseeStage}"  "${directory}/kubernetes/redis-ha" --namespace security --wait
