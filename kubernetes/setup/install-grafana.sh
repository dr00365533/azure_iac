#!/bin/bash
az aks get-credentials --resource-group "${aksResourceGroup}" --name "${aksName}" --admin --overwrite-existing
echo "Installing Grafana"
kubectl create namespace observability
helm upgrade --install grafana  "${directory}/kubernetes/grafana" --namespace observability --wait

