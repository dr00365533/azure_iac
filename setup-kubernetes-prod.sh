#Global
directory="${PWD}"
propheseeStage="dev"
propheseeGeneralKVName="PropheseeProd0001"

#Keyvault
keyvaultName="PropheseeProd0001"
keyvaultResourceGroup="PropheseeSecurity"

#Azure Kubernetes Service
aksResourceGroup="PropheseeKubernetes"
aksName="PropheseeProd0001"
aksServicePrincipalName="PropheseeProdAKS"

#Identity Paramters
podIdentityName="${aksName}-pod-identity"

#Setup Stage
. ./kubernetes/setup/install-stage.sh

#Setup Istio ServiceMesh
. ./kubernetes/setup/install-istio.sh

#Setup Session Redis
# . ./kubernetes/setup/install-redis-ha.sh

#Setup Ambassador Gateway
#  . ./kubernetes/setup/install-ambassador.sh

