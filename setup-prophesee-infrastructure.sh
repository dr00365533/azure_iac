#Global
directory="${PWD}"
tenantId="9a0faec6-0b5d-40c3-a6cb-b7d7dff32549"
subscriptionId="72fd34a7-826d-4fbf-bbd3-a836ff9eb9a6"
propheseeStage="production"
propheseeGeneralKVName="PropheseeProd0001"

#Keyvault
keyvaultName="PropheseeProd0001"
keyvaultResourceGroup="PropheseeSecurity"

#Prophesee Azure Kubernetes Service
aksDnsName="PropheseeProd0001"
aksKubeVersion="1.14.8"
aksMaxPods="250"
aksName="Prophesee"
aksNetworkPlugin="azure"
aksNetworkPrefixes="82.0.0.0/8"
aksNodeCount="3"
aksMinNodeCount="3"
aksMaxNodeCount="6"
aksResourceGroup="PropheseeKubernetes"
aksServicePrincipalName="PropheseeProdAKS"
aksSubnetName="prophesee-prod-aks-0001"
aksSubnetPrefix="82.1.0.0/16"
aksVmSize="Standard_D4s_v3"
vmSetType="VirtualMachineScaleSets" # VirtualMachineScaleSets or  AvailabilitySet
loadBalancerSKU="standard" #basic or standard

#Container Registry
containerRegistryName="prophesee"
containerRegistryResourceGroup="PropheseeGeneral"
containerRegistrySKU="standard"
#CosmosDB
cosmosdbKind="Core"
cosmosdbLocations="westeurope=0"
cosmosdbName="prophesee-saas-prod"
cosmosdbResourceGroup="PropheseeStorage"

#Network
dnsName="prophes.ee"
networkName="prophesee"
networkResourceGroup="PropheseeNetwork"

#Identity Paramters
serverAppName="AKSAADServer"
clientAppName="AKSAADClient"
podIdentityName="${aksName}-aks-pod-identity"


#Create 3rdi Azure Container Registry
. ./azure/acr/create.sh

#Create Azure AD Server App
. ./azure/security/azureadapplications/server/create.sh

#Create Azure AD Client App
. ./azure/security/azureadapplications/client/create.sh

#Create Azure Kubernetes Service Principal
. ./azure/security/aks/serviceprincipal/create.sh

#Create Azure Kubernetes Service Azure AD Identity
. ./azure/security/aks/aadidentity/create.sh

#Create Azure Kubernetes Service Network
. ./azure/network/create.sh

#Create Azure Kubernetes Service Network Subnet
. ./azure/network/subnet/create.sh

#Create Azure Kubernetes Service
. ./azure/aks/create.sh
